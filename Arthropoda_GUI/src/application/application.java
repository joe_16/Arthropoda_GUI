package application;

import java.util.ArrayList;
import java.util.Scanner;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author tobias
 */
public class application extends Application {

	Arthropoda arth;
	Rectangle rechteck;

	public void start(Stage primaryStage) {

		arth = new Arthropoda(4, 2, 100, 100);

		rechteck = new Rectangle(50, 20);

		Group root = new Group();
		root.getChildren().add(rechteck);

		Scene scene = new Scene(root, 250, 250);

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {

				KeyCode gedrückteTaste = event.getCode();

				switch (gedrückteTaste) {
				case W:
					arth.position.setYpos(arth.position.getYpos() - 3);
					break;
				case A:
					arth.position.setXpos(arth.position.getXpos() - 3);

					break;
				case S:
					arth.position.setYpos(arth.position.getYpos() + 3);

					break;
				case D:
					arth.position.setXpos(arth.position.getXpos() + 3);

					break;

				}

				oberflaecheAktualiesierung();

			}

		});

		primaryStage.setTitle("Hello World!");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	public void oberflaecheAktualiesierung() {

		rechteck.setLayoutY(arth.position.getYpos());
		rechteck.setLayoutX(arth.position.getXpos());

	}

}