package application;

import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;

public class GUI_1 extends Application {

	Group root;

	@Override
	public void start(Stage primaryStage) {

		root = new Group();

		ArrayList<Arthropoda> arthropoda = new ArrayList<>();

		int anzahl = Integer
				.parseInt(JOptionPane.showInputDialog("Geben Sie ein wie viele Arthropoda erstellt werden sollen"));

		for (int i = 1; i <= anzahl; i++) {
			int xPos = Integer
					.parseInt(JOptionPane.showInputDialog("Geben Sie die x-Position des: " + i + " Arthropodas ein"));
			int yPos = Integer
					.parseInt(JOptionPane.showInputDialog("Geben Sie die y-Position des:" + i + " Arthropodas ein"));
			Arthropoda arth = new Arthropoda(200, 100, 50, 20);
			arth.position.setXpos(xPos);
			arth.position.setYpos(yPos);
			Rectangle rect = new Rectangle(xPos, yPos, 30, 10);
			root.getChildren().add(rect);
		}

		Scene scene = new Scene(root, 400, 400, Color.WHITE);

		primaryStage.setTitle("Arthropoda");
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}
