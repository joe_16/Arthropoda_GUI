package application;



public class Arthropoda {

	
	Position position;
	private int xpos;
	private int ypos;

	private int eyesN;
	private int feetN;
	private int healthLevel;
	private int hungerLevel;
	private int vergangeneZeit;

	public Arthropoda(int feetN, int eyesN, int healthLevel, int hungerLevel) {
		position = new Position();
		this.feetN = feetN;
		this.eyesN = eyesN;
		this.healthLevel = healthLevel;
		this.hungerLevel = hungerLevel;
		xpos = 0;
		ypos = 0;
	}

	



	public String toString() {

		String leerzeichen = "";

		for (int i = 0; i < xpos; i++) {

			leerzeichen = leerzeichen + " ";

		}

		int beineProSeite = getFeetN() / 2;
		int k�rperL�nge = beineProSeite * 3;

		String k�rper = feetString(k�rperL�nge, false);
		String f��e = feetString(beineProSeite, true);

		String arthropoda = leerzeichen + f��e + "\n" + leerzeichen + k�rper + "\n" + leerzeichen + f��e;

		return arthropoda;

	}

	public void eat(int quantity) {

		setHungerLevel(hungerLevel - quantity);
		if (quantity < 0) {

			setHungerLevel(0);
		}

	}

	public void time(int seconds) {

		int vergangeneZeit = 0;

		vergangeneZeit += seconds;

		setVergangeneZeit(getHungerLevel() + vergangeneZeit % 60);
		setHungerLevel(getHungerLevel() + getVergangeneZeit());

		if (getHungerLevel() > 100) {

			setHungerLevel(0);

			if (getHungerLevel() == 100) {

				setVergangeneZeit(getHungerLevel() - vergangeneZeit % 60);

				setHungerLevel(getHungerLevel() - getVergangeneZeit());

				if (getHungerLevel() == 100) {

					setHungerLevel(0);
				}

			}

		}

	}

	private String feetString(int n, boolean macheF��e) {

		String feetString = "";
		String element;

		if (macheF��e) {
			element = " | ";
		} else {
			element = "#";
		}

		for (int i = 0; i < n; i++) {
			feetString = feetString + element;
		}

		if (!macheF��e) {
			feetString = "<" + feetString + ">";
		} else {
			feetString = feetString + "  ";
		}

		return feetString;
	}

	public enum Direction {
		BACKWARD, FORWARD, UPWARD, DOWNWARD;
	}

	public void moveForward(int dist, Direction direction) {
		
		xpos = xpos + dist;

		switch (direction) {
		case BACKWARD:
			position.setXpos(position.getXpos() - dist);
			break;

		case FORWARD:
			position.setXpos(position.getXpos() + dist);
			break;

		case UPWARD:
			position.setYpos(position.getYpos() - dist);
			break;

		case DOWNWARD:
			position.setYpos(position.getYpos() + dist);
			break;

		default:
			System.out.println("No direction given.");
			break;
		}

	}

	public int getXpos() {
		return xpos;
	}

	public void setXpos(int xpos) {
		this.xpos = xpos;
	}

	public int getYpos() {
		return ypos;
	}

	public void setYpos(int ypos) {
		this.ypos = ypos;
	}

	public int getVergangeneZeit() {
		return vergangeneZeit;
	}

	public void setVergangeneZeit(int vergangeneZeit) {
		this.vergangeneZeit = vergangeneZeit;
	}

	public int getEyesN() {
		return eyesN;
	}

	public void setEyesN(int eyesN) {
		this.eyesN = eyesN;
	}

	public int getFeetN() {
		return feetN;
	}

	public void setFeetN(int feetN) {
		this.feetN = feetN;
	}

	public int getHealthLevel() {
		return healthLevel;
	}

	public void setHealthLevel(int healthLevel) {
		this.healthLevel = healthLevel;
	}

	public int getHungerLevel() {
		return hungerLevel;
	}

	public void setHungerLevel(int hungerLevel) {
		this.hungerLevel = hungerLevel;

	}

	
	}

	
