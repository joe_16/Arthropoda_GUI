package application;

public class Position {

	private int xpos;
	private int ypos;

	public int getXpos() {
		return xpos;

	}

	public void setXpos(int xpos) {
		this.xpos = xpos;
	}

	public int getYpos() {
		return ypos;
	}

	public void setYpos(int ypos) {
		this.ypos = ypos;
	}

}
